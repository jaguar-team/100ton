<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-5 text-center-sm col-xs-12 footer-logo">
					<a href="#top" title="">
						<div class="fully">
							<img src="<?= get_template_directory_uri().'/img/logo_footer.jpg' ?>" alt="logo" />
						</div>
					</a>
					<p class="noSpace">Аренда и услуги спецтехники в ХМАО, ЯНАО</p>
				</div>
				<div class="col-lg-3 col-md-3 col-md-offset-1 col-sm-7 text-center-sm col-sx-12 footer-nav">
					<?php $args = array( // опции для вывода нижнего меню, чтобы они работали, меню должно быть создано в админке
						'theme_location' => 'bottom', // идентификатор меню, определен в register_nav_menus() в function.php
						'container'=> false, // обертка списка, false - это ничего
						'menu_class' => 'nav nav-pills bottom-menu', // класс для ul
				  		'menu_id' => 'bottom-nav', // id для ul
				  		'fallback_cb' => false
				  	);
					wp_nav_menu($args); // выводим нижние меню
					?>
				</div>
				<div class="col-lg-4 col-md-5 col-sm-12 text-center-sm col-xs-12 footer-contact-info">
					<h4>Контактная информация</h4>
					<ul>
						<li class="noSpace"><i class="fa fa-map-marker"></i> г. Новый Уренгой, ул. Сибирская 85, кв. 1</li>
						<li class="min-padding-top"><i class="fa fa-mobile-phone"></i>8 915 528 97 89</li>
						<li class="min-padding-bottom"><i class="fa fa-mobile-phone"></i>8 909 196 01 95</li>
						<li><i class="fa fa-envelope-o"></i> <a href="mailto:Spec_avto_stroi@mail.ru">Spec_avto_stroi@mail.ru</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm footer-bootom-copyright">
						COPYRIGHT © 2008-2016, КОМПАНИЯ "СПЕЦ АВТО СТРОЙ".
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm footer-bootom-developer">
						<img src="<?= get_template_directory_uri().'/img/jaguar-logo.png' ?>" alt="jaguar-team" width="50px" />
						Сайт разработан <a href="http://jaguar-team.com" title="Jaguar-team">Jaguar-team</a>
					</div>
				</div>
			<div>
		</div>
	</footer>

	<!-- modals -->
	<div id="order-modal" class="modal container order-modal" style="display: none;">

		<div class="">
			<?php echo do_shortcode('[contact-form-7 id="140" title="Заказать онлайн"]'); ?>
			<!--<form>
				<textarea class="form-control" placeholder="ОПИШИТЕ ЗАКАЗ*"></textarea>
				<input type="text" name="" class="form-control" placeholder="КОНТАКТНОЕ ЛИЦО*">
				<input type="text" name="" class="form-control tel" placeholder="ТЕЛЕФОН*">
				<input type="submit" name="" class="btn btn-info" style="width: 100%;">
			</form>-->
			<p class="noticeForForm">
				* - Поля, обязательные для заполнения
			</p>
		</div>
	</div>

	<div id="review-modal" class="modal container review-modal" style="display: none;">
		
	</div>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>