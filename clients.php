<?php
/**
 * Template Name: Clients
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>

<?php get_template_part('part/breadcrumbs'); ?>

<div class="main white">
	<div class="client-container">
		<?php foreach(get_posts(array('category_name' => 'clients', 'numberposts' => -1,)) as $value){
			$info = get_post_meta($value->ID); 
		?>
		<div class="client-block">
			<div class="client-inner">
				<a href="<?=$info['linkspost']['0']; ?>">
					<?=get_the_post_thumbnail($value->ID); ?>
				</a>
			</div>
			
		</div>
	
		<?php } ?>
	</div>
	
</div>
	
<section class="section-slider-order-now">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h3 class="text-right text-center-sm" style="font-size: 23px;">Закажите технику по телефону <b style="white-space:nowrap">8 915 528 97 89</b> или </h3>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="text-left text-center-sm">
                     <button class="btn btn-default order">Заказать Онлайн</button>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>