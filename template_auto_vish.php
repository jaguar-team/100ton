<?php
/**
 * Template Name: Автовышки
 */
?>

<?php get_header();?>

<?php get_template_part('part/breadcrumbs'); ?>
<section class="dark-colored-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul id="project-filter" class="project-filter pull-left">
					<li class="textitem">Услуги спецтехники:</li>					
					<li class="active">Автовышки</li>
					<?php foreach(get_pages(array('parent' => 7)) as $key => $value):?>
					<?php if($value->post_title != 'Автовышки'){ ?>
					<li><a href="<?= get_permalink($value->ID); ?>"><?=$value->post_title;?></a></li>
				<?php } endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page-service-detail">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page-service-detail-desc">
				<p>При ремонте коммуникаций на высоте, чистке крыш, покраске зданий и сооружений, наша компания использует автогидроподъемники(АГП) от 18 до 28 метров на базе автомобилей ЗИЛ, УРАЛ,КАМАЗ различной проходимости.</p>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page-service-slider-desc table-responsive">
			    <div class="page-service-slider-desc-block">
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/1.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/1.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/2.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/3.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/4.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/5.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page-service-price">
				<p style="text-align: center; font-size: 28px; font-weight: 400; color: #555;">Стоимость аренды техники</p>
				<div class="table-responsive">
					<table class="table priceTable">
						<thead>
							<tr>
								<th>НАИМЕНОВАНИЕ</th>
								<th>1 МЕСЯЦ И БОЛЕЕ, РУБ./ЧАС</th>
								<th>10 ДНЕЙ И БОЛЕЕ, РУБ./ЧАС</th>
								<th>ДО 3 ДНЕЙ, РУБ./ЧАС</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Автовышка АГП 18м</td>
								<td>1000</td>
								<td>1200</td>
								<td>1400</td>
							</tr>
							<tr>
								<td>Автовышка АГП 18м</td>
								<td>1000</td>
								<td>1200</td>
								<td>1400</td>
							</tr>
							<tr>
								<td>Автовышка АГП 18м</td>
								<td>1000</td>
								<td>1200</td>
								<td>1400</td>
							</tr>
							<tr>
								<td>Автовышка АГП 18м</td>
								<td>1000</td>
								<td>1200</td>
								<td>1400</td>
							</tr>
							<tr>
								<td>Автовышка АГП 18м</td>
								<td>1000</td>
								<td>1200</td>
								<td>1400</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
</section>

<?php get_template_part('part/order-now-row'); ?>

<section class="page-service-completed-work completed-work-container">     
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><span>Выполненные работы</span></h2>
            </div>
        </div> 
        <div class="row">
	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/1.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/2.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/3.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/4.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/5.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/6.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/7.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/8.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/9.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/10.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/1.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>
	    </div>
	</div> 
</section>

<?php get_template_part('part/our-client-slider'); ?>

<?php get_footer(); ?>