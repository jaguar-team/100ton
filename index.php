<?php
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?> 

<section id="carousel-example-generic" class="carousel slide home-slider" data-ride="carousel">
  	<!-- Indicators -->
  	<ol class="carousel-indicators">
    	<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    	<li data-target="#carousel-example-generic" data-slide-to="1"></li>
  	</ol>

  	<!-- Wrapper for slides -->
  	<div class="carousel-inner">
    	<div class="item active">
      		<img src="<?= get_template_directory_uri().'/img/slider-home/1.jpg' ?>" alt="..." />
            <div class="carousel-caption">
                <div class="line-one">НЕГАБАРИТНЫЕ ПЕРЕВОЗКИ В ХМАО И ЯНАО:</div>
                <div class="line-two">
                    - Оборудование для нефтегазовой отрасли<br>
                    - Оборудование промышленного назначения<br>
                    - Энергетическое оборудование<br>
                    - Крупногабаритные технические средства<br>
                </div>
                <a href="<?= get_permalink(7); ?>"><button class="btn btn-info">Подробнее</button></a>
            </div>
    	</div>
    	<div class="item">
      		<img src="<?= get_template_directory_uri().'/img/slider-home/2.jpg' ?>" alt="..." />
            <div class="carousel-caption">
                <div class="line-one">АРЕНДА АВТОКРАНОВ В ХМАО И ЯНАО:</div>
                <div class="line-two">
                    - Автокраны грузоподъемностью от 16 до 150 тонн<br>
                    - Краны-манипуляторы грузоподъемностью от 3 до 7 тонн<br>
                    - В нашем парке техника GROVE, LIEBHERR, TADANO, Галичанин, Ивановец и др.<br>                   
                </div>
                <a href="<?= get_permalink(7); ?>"><button class="btn btn-info hidden-xs">Подробнее</button></a>
            </div>
    	</div>
  	</div>

  	<!-- Controls -->
  	<a class="left carousel-control hidden-xs" href="#carousel-example-generic" data-slide="prev">
    	<i class="fa fa-angle-left"></i>
  	</a>
  	<a class="right carousel-control hidden-xs" href="#carousel-example-generic" data-slide="next">
    	<i class="fa fa-angle-right"></i>
  	</a>
</section>

<section class="section-slider-order-now">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h3 class="text-right text-center-sm headerForOrder" style="font-size: 23px;">Закажите технику по телефону <b style="white-space:nowrap">8 915 528 97 89</b> или </h3>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="text-left text-center-sm">
                     <button class="btn btn-default order">Заказать Онлайн</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><span>Компания "СПЕЦ АВТО СТРОЙ"</span></h2>
                <div>
                    <p>ООО ГК «СПЕЦ АВТО СТРОЙ» специализируется на транспортно-экспедиторских услугах и аренде спецтехники на территории ХМАО, ЯНАО. Наши направления - услуги тралов и площадок, автокранов от 16 до 120 тонн, кранов-манипуляторов, автовышек (АГП).</p>
                    <p><a href="<?= get_permalink(5); ?>"><button class="btn btn-info">Подробнее</button></a></p>
                </div>
        </div>
    </div>
</section>

<section class="section-our-service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><span>Наши услуги</span></h2>
            </div>
        </div>                  
         <?php get_template_part('part/services'); ?>
        
    </div>
</section>

<!-- <section class="completed-work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title">
                    <span>Выполненные работы</span>
                    <?php
                        $post = get_posts( array(
                            'numberposts'       => 1,
                            'orderby'           => 'post_date',
                            'order'             => 'DESC',
                            'name'              => 'our-works',
                            'post_type'         => 'page'
                        ) );

                        $id_post = $post[0]->ID;
                    ?>
                    <a href="<?= get_permalink($id_post); ?>" class="see-all">Все работы</a>
                </h2>
            </div>
        </div>
    </div>
    
    <div class="completed-work-slider">
        <?php foreach (get_posts(array('category_name' => 'completed-work', 'numberposts' => -1)) as $value):
            $info = get_post_meta($value->ID);
         ?>
        <div class="completed-work-slider-block">
            <div class="completed-work-slider-img">
                <?=get_the_post_thumbnail($value->ID);?>
                <div class="completed-work-slider-img-hover"><a href="<?= get_permalink($value->ID); ?> " class="btn btn-info">Подробнее</a></div>
            </div>
            <div class="completed-work-slider-content">
                <div class="completed-work-slider-title">
                    <?= $value->post_title; ?>
                </div>
                <div class="completed-work-slider-desc">
                    <?= $info['customer']['0']; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>        
    </div>
        
    </div>
</section> -->

<section class="our-partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="title has-line">Наши клиенты и партнёры</h2>
                <p>Нам доверяют такие компании как</p>
            </div>
        </div>

        <div class="our-partner-slider">
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/1.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/2.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/3.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/4.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/5.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/6.jpg' ?>" alt="" />
            </div>
        </div>
    </div>
</section>

<section class="home-reviews">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                        $post = get_posts( array(
                            'numberposts'       => 1,
                            'orderby'           => 'post_date',
                            'order'             => 'DESC',
                            'name'              => 'reviews',
                            'post_type'         => 'page'
                        ) );

                        $id_post = $post[0]->ID;
                ?>
                <h2 class="title"><span>Отзывы</span><a href="<?= get_permalink($id_post); ?>" class="see-all">Все отзывы</a></h2>
            </div>
        </div>

        <div class="reviews-slider">
            <div class="reviews-slider-block">
                <div class="row reviews-slider-block-comment">
                    <div class="col-md-10">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                    <div class="col-md-2 reviews-slider-block-comment-img">
                        <a href="#" title="Reviews"><img src="<?= get_template_directory_uri().'/img/reviews-comment/1.jpg'; ?>" alt="" /></a>
                    </div>
                </div>
                <div class="row reviews-slider-block-partner">
                    <div class="col-md-12">
                        <img src="<?= get_template_directory_uri().'/img/our-partner/1.jpg' ?>" alt="" />
                        <span><b>Нечаев А.В..</b><br>
                        Административный менеджер ОАО Мегафон</span>
                    </div>
                </div>
            </div>
            <div class="reviews-slider-block">
                <div class="row reviews-slider-block-comment">
                    <div class="col-md-10">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                    <div class="col-md-2 reviews-slider-block-comment-img">
                        <a href="#" title="Reviews"><img src="<?= get_template_directory_uri().'/img/reviews-comment/1.jpg'; ?>" alt="" /></a>
                    </div>
                </div>
                <div class="row reviews-slider-block-partner">
                    <div class="col-md-12">
                        <img src="<?= get_template_directory_uri().'/img/our-partner/1.jpg' ?>" alt="" />
                        <span><b>Нечаев А.В..</b><br>
                        Административный менеджер ОАО Мегафон</span>
                    </div>
                </div>
            </div>
            <div class="reviews-slider-block">
                <div class="row reviews-slider-block-comment">
                    <div class="col-md-10">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                    <div class="col-md-2 reviews-slider-block-comment-img">
                        <a href="#" title="Reviews"><img src="<?= get_template_directory_uri().'/img/reviews-comment/1.jpg'; ?>" alt="" /></a>
                    </div>
                </div>
                <div class="row reviews-slider-block-partner">
                    <div class="col-md-12">
                        <img src="<?= get_template_directory_uri().'/img/our-partner/1.jpg' ?>" alt="" />
                        <span><b>Нечаев А.В..</b><br>
                        Административный менеджер ОАО Мегафон</span>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

</section>
<section>
    <iframe class="hidden-xs" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5441.069564155732!2d76.62463110353575!3d66.08117598167733!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNjbCsDA0JzUyLjIiTiA3NsKwMzcnNDAuMiJF!5e0!3m2!1sru!2sua!4v1476789562197" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>

<?php get_footer(); // подключаем footer.php ?>

