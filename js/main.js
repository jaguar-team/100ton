jQuery(function($){
	function Menu(){
		var top = $(document).scrollTop();
		if (top < 35) {
			$("header").removeClass('float');
			$('.min').hide();
			$('.full').show();							
		}				
		else {
			$("header").addClass('float');
			$('.min').show();
			$('.full').hide();						
		}
	}
	$(document).ready(function(){

		Menu();
			
		$(window).scroll(function() {
			Menu();
		});

		$('.tel').mask('(999)999-9999');
		$('#email').attr('pattern', '/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i');
		$('.order').click(function(){
			$('#order-modal').modal();$('.wpcf7-response-output').hide();
			$( ".wpcf7-not-valid-tip" ).hide();
			$( '.wpcf7-form p:lt(5)' ).show();
			$('.noticeForForm').show();
			$( ".wpcf7-textarea" ).val("");
			$( ".wpcf7-text" ).val( "" );
		});

		$('.show-bigger').click(function(){
			$('#review-modal').children().remove();
			$('#review-modal').append($(this).find('img').clone()).modal();		  
		});
		

		$('#project-filter').find('li:not(.textitem)').click(function(){

			//change color active link's 
			$(this).siblings().removeClass('active');
			$(this).addClass('active');

			//show & hide li with the completed work
			if($(this).attr('data-filter') == '*'){
				$('.completed-work-block').hide();
				$('.completed-work-block').fadeIn( "slow" );
			}
			else{
				$('.completed-work-block').hide();
				$($(this).attr('data-filter')).fadeIn( "slow" );		
			}
		});	
		
	});
});
jQuery( document ).ready(function() {
	jQuery('.completed-work-slider').slick({
	    slidesToShow: 4,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 1500,
	    dots: false,
	    infinite: true,
	    arrows: false,
	    responsive: [
		    {
		      breakpoint: 1199,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
    	]
	});
	jQuery('.page-service-slider-desc-block').slick({
	    slidesToShow: 5,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 1500,
	    dots: true,
	    infinite: true,
	    arrows: false,
	    responsive: [
		    {
		      breakpoint: 990,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 490,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    }
    	]
	});
	jQuery('.our-partner-slider').slick({
	    slidesToShow: 5,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 1500,
	    dots: false,
	    infinite: true,
	    arrows: true,
	    nextArrow: '<div class="our-partner-slider-btn-right"><i class="fa fa-angle-right"></i></div>',
	    prevArrow: '<div class="our-partner-slider-btn-left"><i class="fa fa-angle-left"></i></div>',
	    responsive: [
		    
		    {
		      breakpoint: 990,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 580,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 410,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        arrows: false
		      }
		    }
    	]
	});
	jQuery('.reviews-slider').slick({
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 1500,
	    dots: true,
	    infinite: true,
	    arrows: true,
	    nextArrow: '<div class="our-partner-slider-btn-right"><i class="fa fa-angle-right"></i></div>',
	    prevArrow: '<div class="our-partner-slider-btn-left"><i class="fa fa-angle-left"></i></div>',
	    responsive: [


		    {
		      breakpoint: 480,
		      settings: {		        
		        arrows: false,
		        dots: false
		      }
		    }
    	]
	});
});