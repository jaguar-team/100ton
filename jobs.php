<?php
/**
 * Template Name: Jobs
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>

<?php get_template_part('part/breadcrumbs'); ?>

	<div class="main">
		<section class="dark-colored-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul id="project-filter" class="project-filter pull-left">
							<li class="textitem">Направления:</li>
							<li data-filter="*" class="active" data-title="">Все</li>
							<li data-filter=".cat1" data-title="">Автовышки</li>
							<li data-filter=".cat2" data-title="">Автокраны</li>
							<li data-filter=".cat3" data-title="">Манипуляторы</li>
							<li data-filter=".cat4" data-title="">Тралы/площадки</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="completed-work-container">       
			<div class="completed-work-block cat1">
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/1.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat2">
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/2.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat3">
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/3.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat4">
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/4.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat1">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/5.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat2">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/6.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat3">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/7.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat4">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/8.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat1">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/9.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat2">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/10.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>

			<div class="completed-work-block cat3">	        	
				<div class="completed-work-img">
					<img src="<?= get_template_directory_uri().'/img/our-work/1.jpg'; ?>" alt="">
					<div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
				</div>
				<div class="completed-work-content">
					<div class="completed-work-title">
						Монтаж котельного блока
					</div>
					<div class="completed-work-desc">
						Заказчик: ООО г.Новосибирс
					</div>
				</div>
			</div>
		</section> 
    </div>
	
	<section class="section-slider-order-now">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h3 class="text-right text-center-sm" style="font-size: 23px;">Закажите технику по телефону <b style="white-space:nowrap">8 915 528 97 89</b> или </h3>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="text-left text-center-sm">
                     <button class="btn btn-default order">Заказать Онлайн</button>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>