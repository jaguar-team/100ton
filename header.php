<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/favicon.png" type="image/png">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
	<?php /* Все скрипты и стили теперь подключаются в functions.php */ ?>

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<?php wp_head(); // необходимо для работы плагинов и функционала ?>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-sm-9 col-md-2 col-lg-3 header-logo">
					<a href="<?= get_home_url(); ?>" class="site-logo" title="<?= __('Home') ?>">
						<div class="full">
							<img src="<?= get_template_directory_uri().'/img/logo.jpg' ?>" alt="logo" />
						</div>
						<div class="min hidden-xs">
							<img src="<?= get_template_directory_uri().'/img/logo_min.jpg' ?>" alt="logo" />
						</div>
					</a>
				</div>
				<div class="col-xs-6 col-sm-3 col-md-9 col-lg-9">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topnav">
      						<i class="fa fa-bars"> Меню</i>
    					</button>
					</div>
					
				</div>
				<div class="collapse navbar-collapse top-nav" id="topnav">
						<?php $args = array( // опции для вывода верхнего меню, чтобы они работали, меню должно быть создано в админке
							'theme_location' => 'top', // идентификатор меню, определен в register_nav_menus() в functions.php
							'container'=> false, // обертка списка, тут не нужна
						  	'menu_id' => 'top-nav-ul', // id для ul
						  	'items_wrap' => '<ul id="%1$s" class="col-lg-9 col-md-10 nav navbar-nav %2$s">%3$s</ul>',
							'menu_class' => 'top-menu', // класс для ul, первые 2 обязательны
						  	'walker' => new bootstrap_menu(true) // верхнее меню выводится по разметке бутсрапа, см класс в functions.php, если по наведению субменю не раскрывать то передайте false		  		
					  		);
							wp_nav_menu($args); // выводим верхнее меню
						?>
					</div>
			</div>
		</div>
	</header>