<?php
/**
 * Template Name: Услуги спецтехники
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>

<?php get_template_part('part/breadcrumbs'); ?>

<div class="container-fluid page-services">
	<div class="container">
        <?php get_template_part('part/services'); ?>
	</div>

</div>

<?php get_template_part('part/order-now-row'); ?>

<?php get_footer(); ?>