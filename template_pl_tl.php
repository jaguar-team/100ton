<?php
/**
 * Template Name: Площадки / тралы
 */
?>

<?php get_header();?>

<?php get_template_part('part/breadcrumbs'); ?>
<section class="dark-colored-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul id="project-filter" class="project-filter pull-left">
					<li class="textitem">Услуги спецтехники:</li>	
					<li class="active">Тралы/площадки</li>
					<?php foreach(get_pages(array('parent' => 7)) as $key => $value):?>
					<?php if($value->post_title != 'Площадки / Тралы'){ ?>
					<li><a href="<?= get_permalink($value->ID); ?>"><?=$value->post_title;?></a></li>
				<?php } endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="page-service-detail">
	<div class="container">
		<div class="row">
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive page-service-detail-desc">
				<p>Для перевозки тяжеловесных и негабаритных грузов, автомобильным транспортом, ООО ГК "100 тонн" использует седельные тягачи, полуприцепы и тралы (низко-,средне-,высокорамные) различной грузоподъемности и проходимости, таких марок как: MAN, VOLVO, FREIGHTLINER, КАМАЗ, УРАЛ, КРАЗ. С начала 2012 года нашими услугами по организации и перевозке грузов, негабаритных технических средств и оборудования промышленного назначения, воспользовались передовые компании нефтегазовой, строительной и других отраслей. </p>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page-service-slider-desc">
			    <div class="page-service-slider-desc-block">
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/1.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/1.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/2.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/3.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/4.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			        <div class="completed-work-slider-block">
			            <div class="completed-work-slider-img">
			                <a href=""><img src="<?= get_template_directory_uri().'/img/our-work/5.jpg' ?>" alt="" /></a>
			            </div>
			            <div class="completed-work-slider-content">
			                <div class="completed-work-slider-title">
			                    Монтаж котельного блока
			                </div>
			                <div class="completed-work-slider-desc">
			                    Заказчик: ООО г.Новосибирс
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page-service-price">
				<p style="text-align: center; font-size: 28px; font-weight: 400; color: #555;">Стоимость аренды техники</p>
				<div class="table-responsive">
					<table class="table priceTable">
						<thead>
							<tr>
								<th>Наименование техники</th>
								<th>Стоимость, руб./час</th>
								<th>Стоимость, руб./км</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Тягач вездеход + п/п площадка, 12м</td>
								<td>1500</td>
								<td>100-120</td>
							</tr>
							<tr>
								<td>Тягач вездеход + п/п трал, до 30т</td>
								<td>2500</td>
								<td>140</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>
</section>

<?php get_template_part('part/order-now-row'); ?>

<section class="page-service-completed-work completed-work-container">     
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="title"><span>Выполненные работы</span></h2>
            </div>
        </div> 
        <div class="row">
	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/1.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/2.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/3.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/4.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/5.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/6.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/7.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/8.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/9.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/10.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>

	        <div class="completed-work-block col-xs-12 col-sm-6 col-md-3 col-lg-3 page-service-completed-work-block">	        	
	            <div class="completed-work-img">
	                <img src="<?= get_template_directory_uri().'/img/our-work/1.jpg'; ?>" alt="">
	                <div class="completed-work-img-hover"><a href="#" class="btn btn-info" tabindex="-1">Подробнее</a></div>
	            </div>
	            <div class="completed-work-content">
	                <div class="completed-work-title">
	                    Монтаж котельного блока
	                </div>
	                <div class="completed-work-desc">
	                    Заказчик: ООО г.Новосибирс
	                </div>
	            </div>
	        </div>
	    </div>
	</div> 
</section>

<?php get_template_part('part/our-client-slider'); ?>

<?php get_footer(); ?>