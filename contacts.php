<?php
/**
 * Template Name: Contacts
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>

<?php get_template_part('part/breadcrumbs'); ?>
<div class="main white">
	<div class="row">
		<div class="col-md-6">
            <div class="col-md-12">
    	        <h2 class="title"><span>Контактная информация</span></h2>
    	        <div>
                	<p><strong>Местоположение: </strong>г. Новый Уренгой, ул. Сибирская 85, кв. 1</p>
                    	<p><strong>Телефоны: </strong>8-915-528-97-89, 8-909-196-01-95, 91-38-80</p>
                    	<p><strong>E-mail: </strong><a href="mailto:Spec_avto_stroi@mail.ru">Spec_avto_stroi@mail.ru</a></p>	            
    	        </div>
            </div>
            <div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5441.069564155732!2d76.62463110353575!3d66.08117598167733!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNjbCsDA0JzUyLjIiTiA3NsKwMzcnNDAuMiJF!5e0!3m2!1sru!2sua!4v1476789562197" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>

	        <div class="col-md-6">
            
                <h2 class="title"><span>Написать нам</span></h2>
                <div>        
                	<?php echo do_shortcode('[contact-form-7 id="200" title="Contact form 1"]'); ?>
                    
                    <p style="padding-top: 15px;">
                        * - Поля, обязательные для заполнения
                    </p>
                </div>

	        </div>
    </div>
</div>

<section class="section-slider-order-now forOrderForm">
    <div class="container">
        <div class="row">
            <div class="col-md-8 slider-order-now-desc forOrderForm">
                Закажите технику по телефону <b class="noSpace">8 915 528 97 89 </b> или
            </div>
            <div class="col-md-4 slider-order-now-btn">
                <button class="btn btn-default order">Заказать Онлайн</button>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>