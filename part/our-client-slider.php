<section class="our-partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="title">Наши клие<span>нты</span> и партнёры</h2>
                <p>Нам доверяют такие компании как</p>
            </div>
        </div>

        <div class="our-partner-slider">
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/1.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/2.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/3.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/4.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/5.jpg' ?>" alt="" />
            </div>
            <div>
                <img src="<?= get_template_directory_uri().'/img/our-partner/6.jpg' ?>" alt="" />
            </div>
        </div>
    </div>
</section>