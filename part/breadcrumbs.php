<?php
	$id_page = get_queried_object_id();

	$title = get_the_title($id_page);
?>
<div class="top-image-container">
	<div class="top-image">
		<div class="container">
		    <h2 class="title"><?= $title; ?></h2>                
		    <?php kama_breadcrumbs(); ?>
		</div>
	</div>
</div>