<?php

$post = get_posts( array(
    'numberposts'       => 1,
    'orderby'           => 'post_date',
    'order'             => 'DESC',
    'name'              => 'service-technology',
    'post_type'         => 'page'
) );

$posts = get_posts( array(
    'numberposts'       => -1,
    'orderby'           => 'post_date',
    'order'             => 'DESC',
    'post_parent'       => $post[0]->ID,
    'post_type'         => 'page'

) );
?>
<div class="">
    <?php foreach($posts as $key => $value): ?>
        <div class="our-service-block">
            <a href="<?= get_permalink($value->ID); ?>" title="<?= $value->post_title; ?>">
                <div class="our-service-block-img"><?= get_the_post_thumbnail($value->ID); ?></div>
                <div class="our-service-block-title"><?= $value->post_title; ?></div>
                
            </a>
            <div class="visible-xs our-service-block-text"><?= $value->post_content; ?></div>
            <div class="our-service-desc hidden-xs"><?= $value->post_content; ?></div>
        </div>
    <?php endforeach; ?>
</div>