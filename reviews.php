<?php
/**
 * Template Name: Reviews
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>

<?php get_template_part('part/breadcrumbs'); ?>

	<div class="main white">
		<div class="story-content col-lg-12 col-md-12 col-sm-12 col-xs-12">  
			<div class="story-content-text col-lg-10 col-md-10 col-sm-12 col-xs-12">Сообщаем, что наша организация сотрудничает с ООО «СпецАвтоСтрой» с 2010 года и по настоящее время. За годы совместной работы мы заключили ряд договоров на аренду спецтехники. Техника была предоставлена согласно техническому заданию. Претензий к качеству техники не было. Все необходимые пожелания были учтены и выполнены в срок.</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 show-bigger">
				<a><img src="<?= get_template_directory_uri().'/img/reviews-comment/1.jpg'; ?>"></a>
			</div>
		</div>
		<div class="story-comment col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<div class="row">
				<img class="story-img col-lg-2 col-md-2 col-sm-4 col-xs-5" src="<?= get_template_directory_uri().'/img/our-partner/4.jpg'; ?>">
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
					<strong>Нечаев А.В.</strong><br>
					<span>ООО «СТК «БАРС»</span>
				</div>
			</div>
		</div>

		<div class="story-content col-lg-12 col-md-12 col-sm-12 col-xs-12">  
			<div class="story-content-text col-lg-10 col-md-10 col-sm-12 col-xs-12">
Наша организация арендовала технику у ООО «СпецАвтоСтрой». Техника в хорошем состоянии, когда были технические неполадки с машинами все решалось оперативно и быстро. Продолжаем так же работать с данной организацией.</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 show-bigger">
				<a><img src="<?= get_template_directory_uri().'/img/reviews-comment/2.jpg'; ?>"></a>
			</div>
		</div>
		<div class="story-comment col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<div class="row">
				<img class="story-img col-lg-2 col-md-2 col-sm-4 col-xs-5" src="<?= get_template_directory_uri().'/img/our-partner/3.jpg'; ?>">
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
					<strong>Чечулин М.И.</strong><br>
					<span>ООО «Фаворит»</span>
				</div>
			</div>
		</div>

		<div class="story-content col-lg-12 col-md-12 col-sm-12 col-xs-12">  
			<div class="story-content-text col-lg-10 col-md-10 col-sm-12 col-xs-12">За время нашего сотрудничества ООО «СпецАвтоСтрой» зарекомендовало себя как надежный партнер, всегда своевременно выполняющий свои обязательства и требования.</div>
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 show-bigger">
				<a><img src="<?= get_template_directory_uri().'/img/reviews-comment/3.jpg'; ?>"></a>
			</div>
		</div>
		<div class="story-comment col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<div class="row">
				<img class="story-img col-lg-2 col-md-2 col-sm-4 col-xs-5" src="<?= get_template_directory_uri().'/img/our-partner/4.jpg'; ?>">
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
					<strong>Жданов А.В.</strong><br>
					<span>ООО «УралТранском»</span>
				</div>
			</div>
		</div>
	</div>

		

<?php get_template_part('part/order-now-row'); ?>

<?php get_footer(); ?>